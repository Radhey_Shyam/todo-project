import React from "react";
import TodoHeader from "./TodoHeader";
import Todos from "./Todos";


function App(){
    return (
        <div>
            <TodoHeader />
            <Todos />
        </div>
    )
}
export default App