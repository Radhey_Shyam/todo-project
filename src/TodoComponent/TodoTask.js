import React, { useEffect, useState } from "react";

function TodoTask(props){
    const [todoList, setTodoList] = useState("")

    useEffect(()=>{
        setTodoList(props.text)
    },[props])

    const editTodo=(e,element)=>{
        let y=todoList.findIndex(ele=>ele.id==element.id)
        var newValue=prompt("Enter your task")
        setTodoList([
          ...todoList,todoList[y]['login']=newValue
        ])
      }
  
      const deleteTodo=((element)=>{
        console.log(element.value)
        setTodoList(todoList.filter(ele=>ele.id!=element.id))
      })

return (<div>
        {todoList!=[]?
            <div>
            {todoList.map(element=>{
            return ( 
            <div>
                <div>
                {element.login}
                </div>
                {element.id?
                <div>
                    <button onClick={(e)=>editTodo(e,element)}>Edit</button>
                    <button onClick={(e)=>deleteTodo(element)}>Delete</button>
                </div>
                :null}
            </div>
            )
            })}
            </div>
        :null}     
    </div>
    )
}
export default TodoTask



