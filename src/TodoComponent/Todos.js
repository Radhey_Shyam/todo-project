
import React, { useEffect, useState } from 'react';
import { Route,Routes } from "react-router-dom";
import TodoTask from "./TodoTask";
import Input from "./Input"

function Todos() {
    var [ task, setTask ] = useState('');
    const [ todoList, setTodoList ] = useState([]);
    useEffect(()=>{
      fetch("https://api.github.com/users?since=135").then((data)=>{
        return data.json()
      }).then(res=>{
        return setTodoList(res)
      })
    },[])

    const handleChange=(e)=>{
      setTask(e.target.value)
  }
  
  const addTask=((e)=>{
      if(task!==''){
        const detail={
          id:Math.floor(Math.random()*1000),
          login:task,
          isCompleted:false ,
        }
        setTodoList([...todoList,detail])
      }
  })

  return (
    <div>
        <Routes>
            <Route path='/showdata' element={<TodoTask text={todoList} />} />
            <Route exact path="/inputdata" element={<Input
              handleChange={handleChange}
              addTask={addTask}
            /> } />
         </Routes> 
    </div>
  )   
}

export default Todos
